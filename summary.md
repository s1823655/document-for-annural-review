# 11_Jul_2022
>__Content__
1. We learned research background of Dr. Kostas
 - involve 16/17 Ys student (No. 20)
 - classify users based on their performance/experience/the game challenge level
 - provide recommendation: what is the next challenge level
2. He is working on a different project (explained AI)

>__To Do__
1. Ask access to the BrainHood (check)
2. Design our own experiments

# 04_Jul_2022
>__Content__
1. Questions from IDC.
2. Whether to take into account *math performance* in our research.
 - solution: It depends on children's baseline like where the children go from the intervention. The evaluation can based on the increment of post/after intervention math performance score. What's more we are interested in how these two variables are connecting, such as if following a positive relavence or negative, and so on.
3. Low accurate emotion classifier for children with ASD.
 - solution: we can consider other physiological data such as heart beats, respiration, skin conductance, etc.. (increase the certainty) **(need more study..)**
4. How to deal with inaccurate case (even it happens in low probability)
 - solution: we may consider to combine other metrics **(need more study..)**.
5. Need test sample before deploy in real practical case.
6. Collaborate research with others.

>__To Do__
1. Check physiological measures and check how to get these measures.
2. Reply emails to Kostas/Brosnan. (ask accessing their techiniques)
3. Complete annual year review form.


# 03_Jul_2022
>__Content__
1. Conference: Affective computing conference; Journal of Numerical Cognition
2. Panos's question/recommendation: 
 - why need real-time feedback? 
 - Hard to involve children with similar math performance. (forcus on supporting metacognitive ability only)
3. Juan palo's question/recommendation:
 - Even for high-level ASD, emotion classifier is not accurate for them.
 - get children's idea by their drawing.
 - find if there are emotions can be classified easily.
4. Hint for game design:
 - pixel style
5. Research outcome of "Fun in coding, a multi-modle approach": * relavence to learning outcome
 - Arousal (+)
 - transition of happiness and surprise (+)
 - stress, angre (-) 
# 29_Jun_2022
>__Content__
1. IDC-22 
2. PhD topic potential risk
>__To Do__
1. none
2. Huan paulo: even 95 percent accuracy is annoying (how to deal with the 5% percent); emotion classification for high-level ASD 
3. Transition from one emotion status to another. (relationship between those two variable)
4. If non-linear relationship be considered in the future, I would study emotions mentioned in SRL/MASRL/CVT.

# 20_Jun_2022
>__Content__
1. Generated frame (encode-decode).
2. Presentation for IDC.
>__To Do__
1. Remove mean age from slides.
2. Add motivation in the background slide.
3. Re-order sequence, TD first then ASD

# 13_Jun_2022
>__Content__
1. Structure of IDC conference (full paper sessions, panel, social events).
2. Important person to meet: Dr Jerry fails, Dr Janet Read, Dr Juan Pablo Hourcade, Dr Judith (teaching design for children with ASD), Dr Victomir Kavanovic (education rating staff).
3. There are website to check words' accessibility for children.
4. IDC slides: background (our focuses, what has been done, why metacognition is important, introduce what I want to do, how to push state-of-arts, any sigfinicant contributions, who would care my project, our motivations, the gaps).
5. Correct words: ASD + TD not only ASD.
6. Add potential issues: Can children understand or not?
7. Add questions: any other tools(learning software, care about metacognition, emotion recognition) to use or we need to develop? 
>__To Do__
1. Mix-style experiments.
2. IDC slides.
3. Ethic application.
4. Find what are these researcher (Content.2) working on?

# 06_Jun_2022
>__Content__
1. Ethic application problem.
2. Odd accuracy explanation.
3. Fine-tuning result on styleGAN.
4. Plan for mix-style
>__To Do__
1. Fix children participant sheet.
2. Slides for IDC.
3. Try Mix-style results.
4. VISA appointment on Wednesday.

# 30_May_2022
>__Content__
1. Performance of ResNet50 on muffled dataset.
2. styleGAN (emotional expression generation).
>__To Do__
1. Check result: Pretrained styleGAN on ChildEFES and LIRISCSE.
2. Transfer learning: control generate direction.

# 23_May_2022
>__Content__
1. Ms.c project proposal (2 pages) should be prepared by the October/November.
2. 10 slides, 15 mins presentation for IDC-22.
3. For synthetic facial pictures: what private information should be hided; can generator invisible those private featrues?
4. If we simply hide some features, would it affect NN's accuracy?
5. Tips for preparing IDC-22: question I need to ask; suggestion about my direction.
>__To Do__
1. Prepare the IDC presentation materials by the 20th of June.
2. If we can invisible some features from face?

# 09_May_2022
>__Content__
1. VISA slots
2. ResNet50 on LIRISCSFE
3. GAN data augmentation
>__To Do__
1. Phone + contact in person
2. Try some datasets with short micro facial expressions
3. Spend less time on GAN augmentation

# 02_May_2022
>__Content__
1. Apply portugal schengen visa
2. check ethic status
3. diminish relationship with hospital at any time
4. check whether Dr.Cao can access TD children

# 18_April_2022
In today's meeting, Charaka and I discussed the accuracy of the emotional classifier for TD children and how we cooperate with Dr Cao.
Here are our contents:
NN trained by the original frame is higher than trained by cropped frame (which contains the face area).
High memory space consumed in loading training samples (over 390GB memory required).
*Cooperative research with Dr Cao who working at Shandong University, China.*

Here are our questions:
1. How to choose CNN layers, ResNet50? ResNet125? VGG16? Based on the popularity?
2. How to better solve memory overwhelming problem expect cropping original frames.

# 11_April_2022
>__Content__
1. CNN+NN accuracy is not good: 
ResNet50: 17%;
ResNet50 + Dense layers: 12%;
2. We may consider to confirm relationships between emotion and metacognitive monitoring accuracy.
3. A missed email.
>__To Do__
1. Try to classify fewer emotions, for example, only bordum; surprise; others.
2. Preprocess images, for example, crop the face from original frame.
3. Familair with ethic approval.
4. Build connection with other group which study ASD.

# 04_April_2022
>__Content__
1. In this meeting, Charaka and I discussed the experiment result of re-weighted learning (one transfer learning algorithm) on ChildEFES (one children's emotion dataset). We think the accuracy is not good enough. 
>__To Do__
1. so I will try convolution NN on ChildEFES this week.

# 21_Mar_2022
>__Content__
1. Rephrase extended abstract.
2. line 138, 149, 152, 201.
3. reduce technique words.
4. replace subquestions by short sentences.
5. create RQ 2 to cover RQ 2.1 and RQ 2.2
>__To Do__
to do content list.

# 14_Mar_2022
>__Content__
1. In this meeting, Charaka and I went through the “research question” and “research aim and objectives”. We think it is a systematic review, but there are some things that still need to be improved and some language should be modified. I list the problems we find below:
>__To Do__
1. We should discuss how the accuracy of emotion classification and list approach that measure children’s emotions. I will implement details about this in section 3.1 “research measurement”.
2. I did not explain what self-learning regulation is. I will explain it in section 1.3 “key-terms description”.
3. Some English sentences should be expressed clearly. I will modify my words based on Charaka’s comments.

# 07_Mar_2022
>__Content__
1. term description in research question.
2. show an example of research question.
3. check transfer learning algorithm in image recognition.
>__To Do__
1. modify research questions
2. check loss reweighting's performance

# 28_Feb_2022
>__Content__
1. feedback group provides feedback containing correct/not correct and evaluation of answered question, etc. 
2. control group does not provide feedback, children make postdiction/prediction based on their own judgement.
3. feedback's quality and the way to provide feedback can be an interesting topic to discuss.
4. other ways to determine the emotion, 'IMotion'/ELAN which is based on researchers' judgement on children's questionnaire.
5. 100% accuracy generic NN. 
>__To Do__
1. Finish the literature review.
2. why some personal NN got zeor accuracy.
3. will transfer learning better?
4. change NN's name.

# 21_Feb_2022
>__Content__
1. Face recognition experiment.
2. IDC Doctoral Consortium.
3. 1st year review.
>__To Do__
1. Change the learning dataset for generic NN into 200 which is similar to the personal dataset, to measure the effect of dataset size on accuracy.
NNs may learn from different datasets, for example, a domain dataset and a partial dataset.
2. By the 4th of April, I should prepare a 4 pages document that describes the objective of my research; the expected output of my research; the contribution of my research.
Before starting, please check the structure, language, and storyline of papers in IDC.
3. By the mid of April, prepare a literature review and research questions for 1st-year review. (best do this first)

# 14_Feb_2022
>__Content__
1. Not mining deeply in the accuracy of facial emotional expression.
2. We can find some other methods used to measure face expression, by which we can check the real emotion expressed by children
3. Build ML agent for TD first, then transfering to ASD.

# 07_Feb_2022
>__Content__
1. The reliability of skin conductance response.
2. Features of facial emotion expression.
3. One personalized neural network.
>__To Do__
1. Collect a set of features used to detact FEE.
2. Use OpenFace to detect features.

# 31_Jan_2022
>__Content__
1. Personalize level experiment:
Aim: test the performance of TAMER and Q-learning when the learner's prefered level of difficulty is changing.
Motivation: the learner is growing while working on our intervention in a long term. So I change the prefered level every 200 steps.
Result: imitation learning algorithm TAMER can handle this dynamic changing very well.
2. Specify the problem we are interested in, the Facial Emotional Expression (FEE) which is different from Facial Emotion Recognition (FER).
3. Skin conductance is used to measure emotional arousal (in that paper published in 1999). We discussed whether it is really reliable to measure emotional arousal and plan to check this method by recent research this week.
4. We are not sure about the reply email from Maja, she said they shared the feature of the dataset in Scientific Robot. How can I get it?
>__To Do__
1. Is the Skin Conductance really reliable?
2. If not, any other psychophysiological measurement?
3. Find features of the dataset Maja mentioned.
4. Keep reading papers about FEE.

# 24_Jan_2022
>__Content__
1. Experiment explanation.
2. New finding: the relationship between emotion and metacognitive process.
>__To Do__
   1. Check list 1: Would children with ASD express emotion?
   2. Check list 1.1: Would children with ASD express evidence of emotion?
   3. Check list 2: How to identify emotion in children with ASD?
   4. Check list 2.1: How accurate be detected? How accurate other measures we may use?
   5. Check list 3: Can children with ASD make high-function behaviour?
   
   6. Reading material 1: Psychophysiological responsiveness to the distress of others in children with autism;
   
   7. Reading material 2: Impaired detection of happy facial expressions in autism;
   
   8. Reading material 3: Physiological Detection of Affective States in Children with Autism Spectrum Disorder;
   Important role: Mary Ellen Foster; Her project: Mummer.

# 17_Jan_2022
>__Content__
1. Replace experiment participants with groups of ASD.
2. The structure of the research question should be like RQ1; RQ1.1;RQ2; RQ2.1... and so on.
3. Question three is relating to self-confidence. Go check what is self-efficacy. If I mention a word in my sentence, I should know what is it, why it comes up here.
4. 'better'? what is better?
5. Add explanation after each question, e.g. MAI would be evaluated in terms of ...
6. In the literature survey, add an RL part in 2.2.
7. Add a gap declaration in the background.
>__To Do__
1. Literature review.
2. Experiment with imitation learning.
3. Reading literature about IL.

# 10_Jan_2022
>__Content__
1.  Discussion about the 'research questions'
>__To Do__
1.  Reorganise questions into two big groups:
Group1: From the view of research target - people:
Group2: From the view of research techinique - ML agent:
2.  In each question, which measurement can answer the question? based on quality or quantity.
3.  Think about the comparision.

# 20_Dec_2021
>__Content__
Corrections in background:
1.	‘Academic performance’ is too general, I will replace it with educational intervention’s improvement/benefits.
2.	Claiming verbal mental score (evaluate whether participants can understand words) when I introduce math performance of children with autism.
3.	I will describe the math performance of children with autism based on both sides that when they did great and when they did bad.
4.	When discussing the Edu interventions, I will claim their benefits and challenges.
5.	Replace scheme-based with traditional-based.
6.	Remove funding cost description of metacognition research.
7.	List a few interventions in the field and describe their drawbacks. 
8.	Replace autism trace by autism level.
Corrections in research questions:
1.	Category questions in three groups.
2.	Let the questions listed first can feed the next question listed.
>__To Do__
1. Todos are in Content.

# 13_Dec_2021
>__Content__
1. PhD proposal structure
>__To Do__
1. Outline of proposal

# 06_Dec_2021
>__Content__
1.  We discussed the hard level match approach which used by the Bath research group.
2.  If children with autism made some math errors which did not done by TD, can we distinguish/observe whether these children with autism display anything which reveals their metacognitive monitoring skill is being challenged.
3.  In our system, we should evaluate children's metacognitive monitoring skills before and after engaging in our system.
4.  I would better introduce our PhD project before sending an email to Bath.
5.  I should add a section to compare our system with other systems when our system completed
>__To Do__
1.  Last week's work: Collect state-of-art research about fostering metacognitive monitoring skills of children with autism
2.  I would like to start to prepare our second experiment which aims to monitor the metacognitive process of children with autism.
3.  I will prepare a research proposal for the Bath group as an introduction of myself, before sending my question to them.
4.  Remind 3rd and 5th points.

# 29_Nov_2021
>__Content__
1. 	How to detect children's math skill age? just like testing one's verbal age.
1.   How to evaluate children's academic age?
1.   Discuss with the research group in Bath.
1.   It is unclear the relationship between metacognitive monitoring ability with math skills.
1.   How to test individuals' metacognition ability.
1.   Collect state-of-art works of fostering math skills of children with autism in terms of metacognition, especially metacognitive monitoring ability.
>__To Do__
In this week:
1. 
2. I will search paper about testing children's math skill/academic skill. It is very important, because we hope to reflect the efficiency of metacognitive foster by higher coins earned. If there is math skill difference between participants, then the final score of each group is affected by individual's math skill factor. It can influence our final conclusion that whether metacognitive foster can improve math skill or not.
1. I will prepare an email in which write my interest on their research, and my questions such as the participants' selection procedure. If they would like to talk more, I am curious why Prof. Brosnan don't keep studying metacognition of children with autism. Since I notice, he turned to focus on social skill of children of autism.
1. even though Bath research group has improved participants' math skill by foster their metacognitive monitoring ability, it is unclear why it works. I will go back to their papers again to figure it out. Also, I will look for other papers/articles which discuss the relation between metacognitive monitroing ability and math skill.
1. there are two famous tests about metacognition, one is 'Mean before think' which did by Russell and Hill in 2001; another is 'Pistol shoot experiment' which did by Sir Simon Baron-Cohen's group in 1998. I will look for a test that can provide a value result indicating the perfoemance of one's metacognitive moniroting ability.
1. I will look for other works about improving the math skills of children with autism by fostering metacognition. I will define some improvable spaces based on their works.

# 22_Nov_2021
>__Content__
1. The studying target, the children with ASD, need to be specified.
1. The development of children with ASD is growing with age, such as the logical ability.
Question: when will children start developing metacognition?
1. The explicit declare of difficult level in the game might affect children's performance.
1. The game does not reflect the user's metacognitive monitoring ability.
>__To Do__
1. I will go back to check the participants of research in the bath university and the South-California university; At least I will specify the age, the IQ, the math ability of our research target.
2. I will find some implicit method to declare the current difficulty level, to let users know where they are but affect their confidence for example.
3. I will add a betting procedure to let users choose how confident they feel they give the right answer.

# 15_Nov_2021
>__Content__
1. Experiment 1 has no link to metacognition monitoring.
1. Before implementing all details in the technique, build a low fidelity prototype.
1. Bring user's opinions about my intervention design.
1. Sharing data in the future.
1. Discussion with others.
>__To Do__
1. I will implement the idea in the paper 'Supporting metacognitive 
monitoring in mathematics learning for young people with autism spectrum disorder: A 
classroom-based study' which fosters the metacognitive monitoring ability of children with ASD.
1. I would like to try to use Balsamiq to design a prototype first.
1. I will ask users' opinions of my prototype.
1. it's time to discuss with other experts in the field of 
metacognition and autism study.

# 08_Nov_2021
>__Content__
1. TA opportunity in IPP.
1. RA opportunity next year.
1. Put each week's report in a framework. 
1. Theory baseline of autism.
1. Lack of data to do experiments.
>__To Do__
1. and
2. I would like to apply for IPP's Tutor/Marking work and RA's opportunity. 
*REMIND* schedule working hours in each week, make sure not over the working hour limitation (although, I thought 
it would be hard to over that limit, I'd better keep in mind.)
1. I make a mistake in week 5's report(07_NOV_2021) that I claim 'interesting topic' in the section title.
It is very confusing for the reader what I am doing. *RECLAIM* Personalization is a guideline I am interested in, to build 
our intervention and it is embedded in my research topic relating to meta-cognitive trouble with children with ASD.
1. I would like to read section 2.1.2/2.2.1 in Aurora's PhD thesis firstly, then look into some theories of 
mind to build a theory-based understanding of ASD.
1. considering some potential help/support from Dr Robin Hill, I would like to investigate the possibility of 
linking metacognition performance to eye-tracking experiment. In this way, I can get help from Robin Hill who can share eye tracking 
data set to me.

# 01_Nov_2021
>__Content__
1. 	The experiment results in ‘Relationships between implicit and explicit uncertainty monitoring and mindreading: 
Evidence from autism spectrum disorder’ is interesting, but there are two problems for us
1.	Its research target is adults which are mismatching with our project. Our project would like to focus on 
children with age 7 - 11 since during which children have learned some knowledge and are flexible to be fostered.
2.	The number of participants in the experiment is small. It may be due to the sensitivity of research on people with autism.
1. 	I have not investigated how researchers use technology such as the Bayes method to teach/foster children with autism/build intervention.
>__To Do__
1. I will try to look for research on children. I think I can find relative research by searching 
papers on google scholar with keywords: ‘Metacognition’, ‘ASD’, and ‘Math’.
1. I will divide a part of the time to look for relative papers.

# 25_Oct_2021
>__Content__
1. Discuss topics described in the Weekly report;
2. Plan next week work;
3. Question about Teaching assistant work.
>__To Do__
1. It is acceptable to investigate metacognitive monitoring problems in children with autism. But there are two things 
that need to be done: other works have been done relating to the metacognitive monitoring problem, for example, whether 
other university groups have done similar project; Is there any work can be improved in the future? for example, can RL perform 
better results? if so, how to make it much better? is there any gap saying other groups have not settled down?
2. In the next week or next few weeks, I would try to solve these two things above. On the other hand, keep mind open to find 
other interesting topics.
3. useful link:
http://portal.theon.inf.ed.ac.uk/reports/teachsupp/admin/sessions/now/TP057_Applicant_Summary.shtml
http://www.drps.ed.ac.uk/21-22/dpt/cx_sb_infr.htm
